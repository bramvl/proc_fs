/*
 *	BramVL
 *	ADC Procfs
 *	2013 - 2014 
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/io.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <asm/io.h>
#include <mach/hardware.h>

//These defines are already defined in arch/arm/mach-bcm2708/include/mach/platform.h 
//which is in the sources so it will be compiled with the module

#define BCM2708_PERI_BASE	0x20000000
#define GPIO_BASE		(BCM2708_PERI_BASE + 0x200000)	//GPIO Controller
#define BSC0_BASE		(BCM2708_PERI_BASE + 0x804000)	//I²C Controller

#define GPIO_BLOCK_SIZE		(4*1024)

//I²C Macros
#define BSC_C			*(&BSC0_ADDR + 0x00)	//Control Register
#define BSC_S			*(&BSC0_ADDR + 0x01)	//Status Register
#define BSC_DLEN		*(&BSC0_ADDR + 0x02)	//Data Lengte Register
#define BSC_A			*(&BSC0_ADDR + 0x03)	//Slave Address Register
#define BSC_FIFO		*(&BSC0_ADDR + 0x04)	//Data FIFO Register


//I²C Controller
#define BSC_C_I2CEN		(1 << 15)	//I²C Enable Register
#define BSC_C_INTR		(1 << 10)	//Receive Interrupt
#define BSC_C_INIT		(1 << 9)	//Register waarde voor Send Interrupt Enable
#define BSC_C_INTD		(1 << 8)	//Register waarde voor Ready Interrupt Enable
#define BSC_C_ST		(1 << 7)	//Start Transfer Register
#define BSC_C_CLEAR		(1 << 4)	//Clear FIFO Register
#define BSC_C_READ		1
//

#define START_READ		BSC_C_I2CEN|BSC_C_ST|BSC_C_CLEAR|BSC_C_READ
#define START_WRITE		BSC_C_I2CEN|BSC_C_ST

//I²C Slave
#define BSC_S_CLKT		(1 << 9)
#define BSC_S_ERR		(1 << 8)
#define BSC_S_RXF		(1 << 7)
#define BSC_S_TXE		(1 << 6)
#define BSC_S_RXD		(1 << 5)
#define BSC_S_TXD		(1 << 4)
#define BSC_S_RXR		(1 << 3)
#define BSC_S_TXW		(1 << 2)
#define BSC_S_DONE		(1 << 1)
#define BSC_S_TA		1
//

#define CLEAR_STATUS		BSC_S_CLKT|BSC_S_ERR|BSC_S_DONE

#define ADC_HW_ADDR		0x54		//ADC Hardware address

void start_i2c(void);		//start i2c function prototype
void wait_for_i2c(void);	//wait for i2c function prototype

//Macros used to initialise Hardware pins
#define INP_GPIO(g)		*(&GPIO_ADDR + ((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g)		*(&GPIO_ADDR + ((g)/10)) |= (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) 	*(&GPIO_ADDR + (((g)/10))) |= (((a) <=3?(a) + 4:(a)==4?3:2)<<(((g)%10)*3))
#define GPIO_READ(g)		*(&GPIO_ADDR + 13) &= (1<<(g))
//

//Cast to volatiles
#define GPIO_ADDR		(*(volatile unsigned long *)GPIOADDR)
#define BSC0_ADDR		(*(volatile unsigned long *)BSC0BASE)
//

unsigned long BSC0BASE;
unsigned long GPIOADDR;

//Start I²C function
void start_i2c(){
	INP_GPIO(3);	// Set "SCL" as input
	SET_GPIO_ALT(3, 0);
	INP_GPIO(5);	// Set "SDA" as input
	SET_GPIO_ALT(5,0);
}

//Wait for I²C function
void wait_for_i2c(){
	int timeout = 500;
	while((!((BSC_S) & BSC_S_DONE)) && --timeout)
		msleep(1);
	if(timeout == 0)
		printk(KERN_ALERT "I²C Timed Out\n");
	return;
}

static struct proc_dir_entry *procfs_dir, *procfs_status_entry;

MODULE_LICENSE("Dual BSD/GPL");
#define MODULE_NAME "ADC_MODULE"

//Function which will be called when procfs file is written
int proc_write_status(struct file *file, const char *buffer, unsigned long count, void *data){
	printk(KERN_ALERT "Can't Write to ADC\n");
	return 1;
}

//Function which will be called when procfs file is read
int proc_read_status(char *page, char **start, off_t off, int count, int *eof, void *data){
	//code voor adc I²C hier
	int length;
	char recv_buf[2]; //2 byte buffer used to read ADC
	unsigned result = 0;

	memset(recv_buf,0,sizeof(recv_buf));
	printk("<1> Reading ADC value\n");
	
	BSC_A = ADC_HW_ADDR;
	BSC_DLEN = 2; //Reading 2 Bytes
	BSC_S = CLEAR_STATUS;
	BSC_C = START_READ;

	wait_for_i2c(); 

	recv_buf[0] = BSC_FIFO; //read first byte from First in First out queue
	recv_buf[1] = BSC_FIFO; //read second byte from First in First out queue

	result = ((recv_buf[0]) << 4 | (recv_buf[1]) >> 4);

	printk("<1>ADC result: %d\n",result);
	printk("<1>ADC FIFO DATA: %d %d\n",recv_buf[0],recv_buf[1]);
	length = sprintf(page,"%d\n",result);
//	return 1;
}

//Function which will be called when procfs module is loaded
int adc_init(void){
	printk("<1>Creating ADC Module\n");

	procfs_dir = proc_mkdir(MODULE_NAME,NULL);

	if(procfs_dir == NULL){
		printk(KERN_ALERT "Could not create procfs_dir\n");
		return -1;
	}
	procfs_status_entry = create_proc_entry("status", 0666, procfs_dir);
	if(procfs_status_entry == NULL){
		printk(KERN_ALERT "Could not create procfs_status_entry");
		return -1;
	}

	printk("<1>ADC: Linking functions\n");
	procfs_status_entry->read_proc = proc_read_status;
	procfs_status_entry->write_proc = proc_write_status;

	printk("<1>ADC: IOREMAP\n");

	//REMAP IO;
	GPIOADDR = (unsigned long)ioremap(GPIO_BASE,GPIO_BLOCK_SIZE);
	BSC0BASE = (unsigned long)ioremap(BSC0_BASE,GPIO_BLOCK_SIZE);

	start_i2c();

	return 0;
}

//Function which will be called when procfs module is removed
void adc_exit(void){
	printk(KERN_INFO "Removing ADC module\n");
	remove_proc_entry("status",procfs_dir);
	remove_proc_entry(MODULE_NAME,NULL);
	return;
}

//Creating connection between procfs functions and C functions
module_init(adc_init);
module_exit(adc_exit);
