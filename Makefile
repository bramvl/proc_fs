obj-m:= proc_fs.o

ALL:
	make -C /home/bvl/Documenten/software/embedded-os/linux/ ARCH=arm CROSS_COMPILE=/usr/bin/arm-linux-gnueabi- M="`pwd`" modules
CLEAN:
	make -C /home/bvl/Documenten/software/embedded-os/linux/ ARCH=arm CROSS_COMPILE=/usr/bin/arm-linux-gnueabi- M="`pwd`" clean
